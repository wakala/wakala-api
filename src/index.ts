import express, { Request, Response } from "express"
import cors from 'cors'
import cookieParser from 'cookie-parser'
import { env, morganMiddleware } from "./config"
import Logger from "./lib/logger"
import router from "./routes"

export const app = express()

app.use(cors())
app.use(cookieParser())
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(morganMiddleware)

// routes
app.get("/test", (req: Request, res: Response) => {
    return res.status(200).json({
        message: "Hola mundo"
    })
})
app.use("/api/v1", router)
app.use('*', (req: Request, res: Response) => {
    return res.sendStatus(404)
})

app.listen(env.api.port, () => {
    Logger.info(`App Iniciada escuchando en http://localhost:${env.api.port}`)
})