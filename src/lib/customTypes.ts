import { Prisma } from "@prisma/client"
import { DefaultArgs } from "@prisma/client/runtime/library";

export interface ormResponse {
    error: boolean,
    message: string | null,
    data: Object | null

}

export interface loginUserData {
    username: string,
    password: string
}

export interface genericResponse {
    error: boolean,
    message: string | null,
    data: object | null
}

