import Logger from "../lib/logger"
import { orm } from '../database/index'
import { genericResponse, loginUserData, ormResponse } from "../lib/customTypes"
import * as bcrypt from "bcrypt"
import jwt from 'jsonwebtoken'
import { env } from "../config"

function createGenericResponse(error: boolean, message: string | null, data: Object | null) {
    return { error, message, data } satisfies genericResponse
}

export const userDriver = {
    loginUser: async (username: string, pass: string) => {
        const query: ormResponse | null = await orm.getUser(username)
        if (!query) return createGenericResponse(true, "username no existe", null)
        try {
            const { password, ...restUserInfo } = query.data as loginUserData
            const isSame = await bcrypt.compare(pass, password)
            if (!isSame) return createGenericResponse(true, "Credenciales Incorrectas", null)
            const token = jwt.sign(restUserInfo, env.api.secret, { algorithm: 'HS256' })

            return createGenericResponse(false, "Inicio de sesion exitoso", { token })
        } catch (error) {
            Logger.error(error)
            return createGenericResponse(true, "Error interno", null)
        }
    },
    createUser: async (username: string, password: string) => {
        const hashPass = await bcrypt.hash(password,10)
        const query = await orm.createUser(username,hashPass) 
        if (query.error) return createGenericResponse(query.error, query.message, query.data)
        return query;
    }
}