import fs from "fs/promises"
import { orm } from "../database"

export const wakalaDriver = {
    async createWakala(sector: string, descripcion: string, foto1: string, foto2: string, userId: string) {
        return await orm.createWakala(sector, descripcion, foto1, foto2, userId)
    },
    async voteS(wakalaId: string, isAdd: string) {

        if (isAdd === 'true')
            return await orm.addSi(wakalaId)
        return await orm.addNo(wakalaId)
    },
    async getWakala(id: string) {
        return await orm.getWakala(id);
    },
    async getWakalas() {
        return await orm.getWakalas();
    },
    async addComment(contenido: string, wakalaId: string, userId: string) {
        return await orm.addCommet(contenido, wakalaId, userId);
    },
    async getComments(wakalaid: string) {
        return await orm.getCommets(wakalaid);
    }
}