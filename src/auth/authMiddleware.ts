import { Request, Response, NextFunction } from "express";
import Logger from "../lib/logger";
import jwt from "jsonwebtoken"


export async function authMiddlerware(req: Request, res: Response, next: NextFunction) {
    try {
        const token = req.headers.authorization?.split(" ")[1]
        if (!token) return res.sendStatus(403)
        // req. = jwt.decode(token, { json: true })

    } catch (error) {
        Logger.error(error)
        return res.sendStatus(403)
    }
}