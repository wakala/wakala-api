import { Router, Request, Response } from "express";
import { userDriver } from "../controller/index"

const router = Router()

router.post("/register",async (req: Request, res: Response) => {

    const { username, password } = req.body
    if (!username || !password) return res.status(400).json({ message: "Falta informacion en el request" })
    const result = await userDriver.createUser(username, password);
    const { error, ...data } = result
    if (error) return res.status(500).json(data)
    return res.status(201).json(data)
})

router.post("/login",async (req: Request, res: Response) => {

    const { username, password } = req.body
    if (!username || !password) return res.status(400).json({ message: "Falta informacion en el request" })
    const result = await userDriver.loginUser(username, password);
    const { error, ...data } = result
    if (error) return res.status(500).json(data)
    return res.status(200).json(data)

})

export default router;