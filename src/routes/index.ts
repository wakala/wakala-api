import { Request, Response, Router } from "express";
import wakaleRouter from "./wakala"
import userRouter from "./user"

const router = Router()

router.use("/wakala", wakaleRouter)
router.use("/user", userRouter)
router.get("/test", (req: Request, res: Response) => {
    res.status(200).json({ message: "correct" });
})

export default router;