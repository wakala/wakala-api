import { Router, Response } from "express";
import { wakalaDriver } from "../controller";
import { expressjwt, Request } from "express-jwt"
import type { Algorithm } from "jsonwebtoken";
import multer from "multer"
import { env } from "../config";
import path from "path";
import { ormResponse } from "../lib/customTypes";



const router = Router()

router.route("/image/:name").get((req: Request, res: Response) => {
    if (!req.params.name) return res.sendStatus(400);
    return res.status(200).setHeader("content-type", "image/jpeg").sendFile(path.join(__dirname, "../../", "files", req.params.name));
});

router.use(expressjwt({
    secret: env.api.secret,
    algorithms: [env.api.algorithm as Algorithm]
}))
router.route('').get(
    async (req: Request, res: Response) => {
        const wakala = await wakalaDriver.getWakalas();
        if (wakala.error) return res.status(500).json({ message: wakala.message })
        if (!wakala.data) return res.status(404).json({ message: wakala.message })
        return res.status(200).json({ wakalas: wakala.data })
    }
).post(
    multer({ dest: './files' }).fields([{ name: "fotos", maxCount: 2 }]),
    async (req: Request, res: Response) => {
        const { sector, descripcion } = req.body
        if (!sector || !descripcion) return res.sendStatus(400)
        if (!req.files) return res.sendStatus(400)
        const files = req.files as { [fieldname: string]: Express.Multer.File[] };
        const foto1 = files.fotos[0].filename;
        const foto2 = files.fotos[1] ? files.fotos[1].fieldname : "";


        const wakala = await wakalaDriver.createWakala(sector, descripcion, foto1, foto2, req.auth?.id);

        if (wakala.error) return res.status(500).json({ message: wakala.message })
        if (!wakala.data) return res.status(404).json({ message: wakala.message })

        return res.status(201).send(wakala);
        // return res.status(201).json({ message: "Wakala creado correctamente" })
    }
)

router.route("/:id").get(
    async (req: Request, res: Response) => {
        const wakala = await wakalaDriver.getWakala(req.params.id);
        if (wakala.error) return res.status(500).json({ message: wakala.message })
        if (!wakala.data) return res.status(404).json({ message: wakala.message })
        return res.status(200).json({ wakala: wakala.data })
    }
).put(
    async (req: Request, res: Response) => {
        if (req.body.voto == null || !req.params.id) return res.sendStatus(400)
        const { voto } = req.body
        const wakala = await wakalaDriver.voteS(req.params.id, voto);
        if (wakala.error) return res.status(500).json({ message: wakala.message })
        if (!wakala.data) return res.status(404).json({ message: wakala.message })
        return res.status(200).json({ wakala: wakala.data, message: wakala.message })
    }
)

router.route("/:id/comentarios").get(async (req: Request, res: Response) => {
    if (!req.params.id) return res.sendStatus(400);
    const comentarios: ormResponse = await wakalaDriver.getComments(req.params.id);
    if (comentarios.error) return res.status(500).json({ message: comentarios.message });
    return res.status(200).json({ message: comentarios.message, data: comentarios.data })

}).post(async (req: Request, res: Response) => {
    if (!req.body.contenido || !req.params.id) return res.sendStatus(400);
    const { contenido } = req.body
    const wakala: ormResponse = await wakalaDriver.addComment(contenido, req.params.id, req.auth?.id);
    if (wakala.error) return res.status(500).json({ message: wakala.message });
    if (!wakala.data) return res.status(404).json({ message: wakala.message })
    return res.status(201).json({ message: wakala.message, comentarios: wakala.data });
});

export default router;