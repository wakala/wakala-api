import 'dotenv/config'

export const env = {
    api: {
        port: process.env.PORT || 3000,
        secret: process.env.TOKEN_SECRET || 'banana',
        algorithm: process.env.ALGORITHM || 'HS256'
    }
}