import { db } from ".";
import Logger from "../lib/logger";
import { ormResponse } from "../lib/customTypes";
import { PrismaClientKnownRequestError } from "@prisma/client/runtime/library";
import { Prisma } from "@prisma/client";

function createOrmResponse(error: boolean, message: string | null, data: Object | null | [Object]): ormResponse {
    return { error, message, data } satisfies ormResponse
}
function logPrismaError(error: unknown) {
    if (error instanceof PrismaClientKnownRequestError) {
        Logger.error(error.message)
        return createOrmResponse(true, error.message, null)
    }
    Logger.error(error)
    return createOrmResponse(true, "Error desconocido", null)
}


export const orm = {
    getUser: async (username: string) => {
        try {
            const result = await db.user.findUnique({
                where: {
                    username
                },
            })

            return createOrmResponse(false, null, result);
        } catch (error) {

            return logPrismaError(error)
        }
    },
    createUser: async (username: string, pass: string) => {
        try {
            const result = await db.user.create({
                data: {
                    username: username,
                    password: pass
                },
                select: {
                    username: true,
                    id: true,
                    createdAt: true,
                }
            })
            return createOrmResponse(false, "Se ha creado el usuario exitosamente", result)
        } catch (error) {
            return logPrismaError(error)
        }

    },
    getWakala: async (id: string) => {
        try {
            const result = await db.wakala.findUnique({
                where: {
                    id
                },
                include: {
                    user: {
                        select: {
                            username: true,
                            id: true,
                        }
                    },
                }
            })
            const data = { ...result, si: result?.si.toString(), no: result?.no.toString() };
            return createOrmResponse(false, null, data)

        } catch (error) {
            return logPrismaError(error)
        }
    },
    getWakalas: async () => {
        try {
            const result = await db.wakala.findMany({ include: { user: { select: { password: false, username: true, id: true }, }, }, });
            let data: [Object] = [{}];
            data.pop();
            result.forEach((wakala) => {
                data.push({ ...wakala, si: wakala.si.toString(), no: wakala.no.toString() })
            })

            return createOrmResponse(false, null, data)

        } catch (error) {
            return logPrismaError(error)
        }
    },
    createWakala: async (sector: string, descripcion: string, foto1: string, foto2: string, userId: string) => {
        try {
            const result = await db.wakala.create({
                data: {
                    sector,
                    descripcion,
                    foto1,
                    foto2,
                    userId,
                }
            })
            const data = { ...result, si: result.si.toString(), no: result.no.toString() };
            return createOrmResponse(false, "Se a creado el wakala correctamente", data)
        } catch (error) {

            return logPrismaError(error)
        }
    },
    addSi: async (wakalaId: string) => {

        try {
            const wakala = await db.wakala.update({
                where: { id: wakalaId }, data: {
                    si: {
                        increment: 1
                    }
                },
                include: {
                    user: {
                        select: {
                            username: true,
                            id: true
                        }
                    }
                }

            });

            const data = { ...wakala, si: wakala.si.toString(), no: wakala.no.toString() }
            return createOrmResponse(false, "Se ha actualizado los votos del wakala existosamente", data);
        } catch (error) {
            return logPrismaError(error)
        }

    },
    addNo: async (wakalaId: string) => {
        try {
            const wakala = await db.wakala.update({
                where: { id: wakalaId }, data: {
                    no: {
                        increment: 1
                    }
                },
                include: {
                    user: {
                        select: {
                            username: true,
                            id: true
                        }
                    }
                }
            });
            const data = { ...wakala, si: wakala.si.toString(), no: wakala.no.toString() }
            return createOrmResponse(false, "Se ha actualizado los votos del wakala existosamente", data);
        } catch (error) {
            return logPrismaError(error)
        }

    },
    addCommet: async (contenido: string, wakalaId: string, userId: string) => {
        try {
            const user = await db.user.findUnique({ where: { id: userId } });
            const comentario = await db.comentario.create({
                data: {
                    contenido,
                    userId: userId,
                    wakalaId: wakalaId,
                }
            })
            return createOrmResponse(false, `Se agreago el comentario ${comentario.id} hecho por ${user?.username} a el wakala ${wakalaId}`, comentario);
        } catch (error) {
            return logPrismaError(error);
        }
    },
    getCommets: async (wakalaId: string) => {
        try {
            const comentarios = await db.comentario.findMany({ where: { wakalaId: wakalaId }, include: { user: { select: { username: true, id: true } } } });
            if (comentarios)
                return createOrmResponse(false, `Se obtuvieron todos los comenatios del wakala ${wakalaId}`, comentarios);
            else return createOrmResponse(false, 'No hay comentarios', null);
        } catch (error) {
            return logPrismaError(error);
        }
    }

}